class SenseisController < ApplicationController
  before_action :set_sensei, only: [:show, :update, :destroy]

  def index
    @dojo = Dojo.find(params[:dojo_id])
    @senseis = Sensei.where(dojo_id: @dojo.id)
    render json: @senseis, include: :dojo, status: :ok
  end

  def show
    @sensei = Sensei.find(params[:id])

    render json: @sensei, status: :ok
  end
  
  def create
    @new_sensei = Sensei.new(sensei_params)

    if @new_sensei.save
      render json: @new_sensei, status: created
    else 
      render json: @new_sensei.errors, status: :unprocessable_entity      
    end
  end
  
  def update
    if @sensei.update(sensei_params)
      render json: @sensei
    else
      render json: @sensei.errors, status: :unprocessable_entity
    end
  end
  
  def destroy
    @sensei = Sensei.find(params[:id])
    
    @sensei.destroy
  end
  
  private
  
  def sensei_params
    params.require(:sensei).permit(:wise_quote, :name, :image_url, :dojo_id)
  end

end
