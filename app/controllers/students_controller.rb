class StudentsController < ApplicationController
  before_action :set_students, only: [:show, :update, :destroy]

  def index
    @dojo = Dojo.find(params[:dojo_id])
    @sensei = Sensei.where(dojo_id: @dojo.id)
    @students = Student.where(sensei: @sensei)
    render json: @students, include: :sensei, status: :ok
  end
    
  def show
    # @message = "Show: #{params[:id]}"
    # @student = Student.find(params[:id])

    # @students = Student.all

    render json: @student, include: [dojo, sensei], status: :ok
  end
  
  def create
    @new_student = Student.new(student_params)

    if @new_student.save
      render json: @new_student, status: created
    else 
      render json: @new_student.errors, status: :unprocessable_entity      
    end
  end
  
  def update
    if @student.update(student_params)
      render json: @student
    else
      render json: @student.errors, status: :unprocessable_entity
    end
  end
  
  def destroy
    @student = Student.find(params[:id])

    @student.destroy
  end
  
  private
  
  def set_student
    @student = Student.find(params[:id])
  end


  def student_params
    params.require(:student).permit(:name, :age, :special_attack, :sensei_id)
  end

end
